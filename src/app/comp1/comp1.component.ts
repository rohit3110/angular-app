import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-comp1',
  templateUrl: './comp1.component.html',
  styleUrls: ['./comp1.component.css']
})
export class Comp1Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  person = {
name: "Rohit",
age: 30,
phone: 9910847492,
grade:{
  tenth: 60,
  twelth: 71
}
  }




arr1 = [{
  name: "Rohit",
  age: 30,
  phone: 9910847492,
 },
 {
  name: "Vicky",
  age: 31,
  phone: 9910847492,
 },
 {
  name: "Ramji",
  age: 29,
  phone: 9910847492,
 },
 {
  name: "Amit",
  age: 28,
  phone: 9910847492,
 },
 {
  name: "Vishal",
  age: 25,
  phone: 9910847492,
 }
]

count = 0;
counter(){
console.log(this.count++) 
}


fontSize = 16;

inc(){
  this.fontSize++
}

dec(){
  this.fontSize--
}
reset(){
  this.fontSize = 16;
}

textVal = 'Sample text'

// values:any;
// onKeyUp(){
// this.values += event.target.value + "-";
// console.log(event)
// }
}
