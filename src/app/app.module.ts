import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
//import {RouterModule} from "@angular/router";
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { Comp1Component } from './comp1/comp1.component';
import { DirectivesComponent } from './directives/directives.component';
import { TempleteFormsComponent } from './templete-forms/templete-forms.component';
import { IntroductionComponent } from './introduction/introduction.component';
import { TypescriptComponent } from './typescript/typescript.component';

const AppRoutes: Routes = [
  { path: '',
    redirectTo: '/introduction',
    pathMatch: 'full'
  },
  
  {
    path:"introduction", 
    component: IntroductionComponent,
   
  },
  {
    path:"typescript", 
    component: TypescriptComponent 
  },
  {
    path:"templete-forms", 
    component: TempleteFormsComponent  
  },
  {
    path:"displaying-data", 
    component: Comp1Component  
  },
  {
    path:"directives", 
    component: DirectivesComponent  
  },
  {
    path:"templete-forms", 
    component: TempleteFormsComponent  
  },
  ]


@NgModule({
  declarations: [
    AppComponent,
    Comp1Component,
    DirectivesComponent,
    TempleteFormsComponent,
    IntroductionComponent,
    TypescriptComponent,
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    RouterModule.forRoot(AppRoutes),
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
