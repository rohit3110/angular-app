import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directives',
  templateUrl: './directives.component.html',
  styleUrls: ['./directives.component.css']
})
export class DirectivesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  courses = [1,2,3];

  schoolLlist = [

    {id:1, name:"school 1"},
    {id:2, name:"school 2"},
    {id:3, name:"school 3"},
    {id:4, name:"school 4"},
    {id:5, name:"school 5"},
  ]
  add(){
this.schoolLlist.push({id:6, name:"school 6"})
  }
}
